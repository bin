#!/bin/sh
if which stterm >/dev/null 2>&1; then
    exec stterm -f 'Inconsolata:pixelsize=25';
elif which urxvt >/dev/null 2>&1; then
    exec urxvt -f 'xft:Inconsolata:pixelsize=25';
elif which uxterm >/dev/null 2>&1; then
    exec uxterm;
else
    exec xterm;
fi;
