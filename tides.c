// Yet another (tm) fetch program

#include <unistd.h>
#include <sys/utsname.h>
#include <sys/sysinfo.h>
#include <time.h>

#include <limits.h>
#include <string.h>
#include <stdio.h>

int
main(void)
{
	// Get hostname
	char hostname[HOST_NAME_MAX];
	gethostname(hostname, HOST_NAME_MAX);

	// Get username
	char username[LOGIN_NAME_MAX];
	getlogin_r(username, LOGIN_NAME_MAX);

	// Get current working directory
	char cwd[PATH_MAX];
	getcwd(cwd, PATH_MAX);

	// Get assorted system info
	struct sysinfo info;
	sysinfo(&info);

	// Get uptime
	struct tm *uptime = gmtime(&info.uptime);

	// Get current time
	time_t rawtime;
	time(&rawtime);
	struct tm *wtime = localtime(&rawtime);

	// Get kernel info
	struct utsname kernel;
	uname(&kernel);

	/* Get memory info from /proc. Inspired by busybox free.c, which is
		 GPLv2 licensed */
	char buf[60]; // actual lines we expect are ~30 chars or less
	int counter = 2; // Number of things being scanned for in the file
	unsigned long total_kb, avail_kb;

	FILE *fp = fopen("/proc/meminfo", "r");
	total_kb = avail_kb = 0;
	while (fgets(buf, sizeof(buf), fp)) {
		if (sscanf(buf, "MemTotal: %lu %*s\n", &total_kb) == 1)
			if (--counter == 0)
				break;
		if (sscanf(buf, "MemAvailable: %lu %*s\n", &avail_kb) == 1)
			if (--counter == 0)
				break;
	}
	fclose(fp);

	// Display
	printf("%s@%s\n", username, hostname);
	printf("%s\n", cwd);
	printf("%d-%02d-%02d %02d:%02d:%02d\n", wtime->tm_year + 1900, wtime->tm_mon,
				 wtime->tm_mday, wtime->tm_hour, wtime->tm_min, wtime->tm_sec);
	printf("%0.0f/%0.0f MB RAM\n", (total_kb - avail_kb)/1024.0, total_kb/1024.0);
	printf("%s %s\n", kernel.sysname, kernel.release);
	printf("Up %02dh %02dm %02ds\n", uptime->tm_hour, uptime->tm_min,
				 uptime->tm_sec);
	printf("%0.0f/%0.0f MB RAM\n", (total_kb - avail_kb)/1024.0, total_kb/1024.0);
}
