#!/usr/bin/perl
# SCRIPTNAME DOES_SOMETHING
# and is released under the terms of the GNU GPL version 3, or any
# later version, at your option. See the file README and COPYING for
# more information.
# Copyright 2016 by Don Armstrong <don@donarmstrong.com>.


use warnings;
use strict;

use Getopt::Long;
use Pod::Usage;

use LWP::Simple;
use XML::LibXML;
use POSIX qw(strftime);
use DateTime;
use DateTime::Format::DateParse qw(parse_datetime);

=head1 NAME

SCRIPTNAME - DOES_SOMETHING

=head1 SYNOPSIS

SCRIPTNAME [options]

 Options:
   --debug, -d debugging level (Default 0)
   --help, -h display this help
   --man, -m display manual

=head1 OPTIONS

=over

=item B<--debug, -d>

Debug verbosity. (Default 0)

=item B<--help, -h>

Display brief usage information.

=item B<--man, -m>

Display this manual.

=back

=head1 EXAMPLES

SCRIPTNAME

=cut


use vars qw($DEBUG);

my %options = (debug           => 0,
               help            => 0,
               man             => 0,
              );

GetOptions(\%options,
           'debug|d+','help|h|?','man|m');

pod2usage() if $options{help};
pod2usage({verbose=>2}) if $options{man};

$DEBUG = $options{debug};

my @USAGE_ERRORS;
# if (1) {
#     push @USAGE_ERRORS,"You must pass something";
# }

pod2usage(join("\n",@USAGE_ERRORS)) if @USAGE_ERRORS;


my $xml = XML::LibXML->load_xml(string => get('https://debconf16.debconf.org/schedule/pentabarf.xml'));

print << 'EOF';
BEGIN:VCALENDAR
VERSION:2.0
X-WR-CALNAME:pentabarf_to_ics
PRODID:-//PentabarfToICS//EN
X-WR-TIMEZONE:UTC
X-WR-CALDESC:Pentabarf To ICS
CALSCALE:GREGORIAN
EOF
my $timestamp = strftime('%Y%m%dT%H%M%SZ',gmtime);
for my $event ($xml->findnodes('.//event')) {
    print STDERR $event->toString if $DEBUG;
    my $id = $event->getAttribute('id');
    my $start = DateTime::Format::DateParse->parse_datetime(node_content($event,'date'));
    $start->set_time_zone('UTC');
    my $start_stamp = strftime('%Y%m%dT%H%M%SZ',gmtime($start->epoch));
    my $duration = node_content($event,'duration');
    if (not defined $duration or $duration eq ':') {
        $duration = node_content($xml,'timeslot_duration');
    }
    my ($h,$m) = $duration =~ /(\d+)\:(\d+)/;
    $duration = DateTime::Duration->new(hours => $h,minutes => $m);
    my $stop_stamp = strftime('%Y%m%dT%H%M%SZ',gmtime(($start+$duration)->epoch));
    my $summary = node_content($event,'title');
    my $description = node_content($event,'description');
    $description =~ s/\n/\\n/g;
    my $url = node_content($event,'full_conf_url');
    my $type = node_content($event,'type');
    print <<"EOF";
BEGIN:VEVENT
DTSTAMP:$timestamp
UID: $id
DTSTART: $start_stamp
DTEND: $stop_stamp
SUMMARY: $summary
DESCRIPTION:: $description
 $url
CATEGORIES: $type
END:VEVENT
EOF
}


print << 'EOF';
END:VCALENDAR
EOF


sub node_content {
    my ($xml,$node) = @_;

    my ($n) = $xml->findnodes(".//$node");
    return $n->textContent();
}

__END__
