get-check(1)

# NAME
git-check - print a summary of Git repos in a directory

# SYNOPSIS
*git* check [directory]

# DESCRIPTION
Prints a diffstat for each repo in $PWD or given directory.

# COPYRIGHT
This is free and unencumbered software released into the public domain. For more
information, please refer to <https://unlicense.org/>.