// CGI program to get fortunes. Powers https://fortune.armaanb.net.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main(void) {
    const char *path = getenv("PATH_INFO");
    FILE *fp;
    char buf[255];

    if (strcmp(path, "/latin-cowsay") == 0) {
        fp = popen("phrases | cowsay", "r");
    } else if (strcmp(path, "/cowsay") == 0) {
        fp = popen("fortune | cowsay", "r");
    } else if (strcmp(path, "/latin") == 0) {
        fp = popen("phrases", "r");
    } else if (strcmp(path, "/") == 0) {
        fp = popen("fortune", "r");
    } else {
        printf("Status: 404"
               "Content-type: text/plain\n\n"
               "Page not found. Try /, /cowsay, /latin, or /latin-cowsay\n");
        return 0;
    }

    printf("Content-type: text/plain\n\n");
    while (fgets(buf, sizeof(buf), fp) != NULL) {
        printf("%s", buf);
    }
    pclose(fp);

    return 0;
}
