/* Convert ASCII to morse code. Reads from stdin if provided with no argument 	
 or "-", otherwise reads from the first command line argument. Prints "?" if 	
 character is not found. */  

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

const char *aa[] = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
										"k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
										"u", "v", "w", "x", "y", "z", "1", "2", "3", "4",
										"5", "6", "7", "8", "9", "0", " "};

const char *ma[] = {"•-", "-•••", "-•-•", "-••", "•", "••-•", "--•",
										"••••", "••", "•---", "-•-", "•-••", "--", "-•",
										"---", "•--•", "--•-", "•-•", "•••", "-", "•--",
										"•••-", "•--", "-••-", "-•--", "--••", "•----",
										"••---", "•••--", "••••-", "•••••", "-••••",
										"--•••", "---••", "----•", "-----", " "};

void
convert(char *c)
{
	if (strcmp(c, "\n") == 0) {
		printf("\n");
		return;
	}

	for (int i = 0; aa[i]; i++) {
		if (strcmp(c, aa[i]) == 0) {
			printf("%s", ma[i]);
			break;
		} else if (i == sizeof(aa)/sizeof(char *) - 1) {
			printf("?");
		}
	}
	printf(" ");
}

int
main(int argc, char **argv)
{
	(void)argc;
	char c[2];
	c[1] = '\0';

	if (argv[1] && strcmp(argv[1], "-") != 0) {
		for (int i = 0; argv[1][i]; i++) {
			c[0] = argv[1][i];
			convert(c);
		}
	} else {
		while (read(STDIN_FILENO, &c[0], 1) > 0) {
			convert(c);
		}
	}
	printf("\n");
	return 0;
}
