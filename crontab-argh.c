/* Stops you from accidentally running crontab -r. alias this to crontab.
         Alternatively, if you are on a GNU system, you can just alias "crontab
   -r" to "crontab -ri", but this isn't POSIX. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main(int argc, char *argv[]) {
    char arg[255] = "crontab ";

    for (int i = 1; i < argc; i++) {
        strcat(arg, strcat(argv[i], " "));
    }

    if (strstr(arg, "-r") != NULL) {
        printf("%s", "Delete user's crontab? [yes/no]: ");
        char inp[255];
        int ret;
        while (1) {
            ret = scanf("%s", inp);
            if (ret == EOF)
                continue;
            else if (strcmp(inp, "no") == 0)
                exit(0);
            else if (strcmp(inp, "yes") == 0)
                break;
            else
                printf("%s", "Please answer yes or no. [yes/no]: ");
        }
    }
    system(arg);
}
