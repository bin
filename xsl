#!/bin/sh

if [ -n "$SSH_AUTH_SOCK" ]; then
    echo "Removing ssh identities"
    ssh-add -D;
fi;

# lock the screen
if [ -n "$DISPLAY" ]; then
    if [ -x /usr/bin/xscreensaver-command ]; then 
	/usr/bin/xscreensaver-command -lock;
    elif [ -x /usr/bin/xlock ]; then 
	/usr/bin/xlock
    fi;
else
    if [ -x /usr/bin/vlock ]; then
	vlock
    fi;
fi;

	