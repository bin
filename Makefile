CFLAGS := -Wall -Wextra -pedantic -D_POSIX_C_SOURCE=200112L ${CFLAGS}
PREFIX ?= /usr/local

all: setup bin man

bin:
	for i in *.c; do \
		echo "CC\t$$i"; \
		$(CC) $(CFLAGS) -o build/bin/$$(basename "$$i" .c) "$$i"; \
	done

man:
	for i in man/*.scd; do \
		echo "SCDOC\t$$i"; \
		scdoc < $$i > build/man/$$(basename "$$i" .scd); \
	done

setup:
	mkdir -p build/bin build/man

install: all man-install
	cp -r build/bin/* $(DESTDIR)/$(PREFIX)/bin/

man-install:
	cp -r build/man/* $(DESTDIR)/$(PREFIX)/share/man/man1/

clean:
	rm -rf build

.PHONY: bin man clean install
