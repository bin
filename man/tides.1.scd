tides(1)

# NAME
tides - no BS fetch program

# SYNOPSIS
*tides*

# DESCRIPTION
Yet Another (tm) crappy little fetch program. No real practical uses, just use
pfetch or ufetch or whatever the kids are doing nowadays.

# COPYRIGHT
This is free and unencumbered software released into the public domain. For more
information, please refer to <https://unlicense.org/>.