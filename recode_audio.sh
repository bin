#!/bin/sh

set -e

for a in "$@"; do
    echo $a; 
    mencoder $a -o ${a%%.mpg}.avi -ovc copy -oac lavc -lavcopts acodec=mp3
    mplayer -dumpfile ${a%%.mpg}.mp3 -dumpaudio ${a%%.mpg}.avi
done;