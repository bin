#! /usr/bin/perl
# , and is released
# under the terms of the GPL version 2, or any later version, at your
# option. See the file README and COPYING for more information.
# Copyright 2006 by Don Armstrong <don@donarmstrong.com>.
# $Id: perl_script 495 2006-08-10 08:02:01Z don $


use warnings;
use strict;

use Getopt::Long;
use Pod::Usage;

=head1 NAME

 - 

=head1 SYNOPSIS

 [options]

 Options:
  --debug, -d debugging level (Default 0)
  --help, -h display this help
  --man, -m display manual

=head1 OPTIONS

=over

=item B<--debug, -d>

Debug verbosity. (Default 0)

=item B<--help, -h>

Display brief useage information.

=item B<--man, -m>

Display this manual.

=back

=head1 EXAMPLES


=cut


use vars qw($DEBUG);
use IO::File;

my %options = (debug           => 0,
	       help            => 0,
	       man             => 0,
	       multiplier      => 1,
	       add             => 0,
	      );

GetOptions(\%options,'debug|d+','help|h|?','man|m',
	   'gradebook=s','ilearn_gradebook|ilearn-gradebook=s',
	   'gradebook_column|gb-col=s','ilearn_column|il-col=s','multiplier|mul=s',
	   'add|addition=s',
	  );

pod2usage() if $options{help};
pod2usage({verbose=>2}) if $options{man};

$DEBUG = $options{debug};

# read in the gradebook

my $gb = new IO::File $options{gradebook},'r' or
     die "Unable to open $options{gradebook} for reading: $!";

my %gb_students;
while (<$gb>) {
     chomp;
     next unless /^\d+/;
     my ($num,$name,$id,@columns) = split /\t/;
     next unless defined $name;
     $id =~ s/[^\d]+//g;
     die "No student id for $name at line $." if not length $id;
     $gb_students{$id} = {name => $name,
			  score => (defined $columns[$options{gradebook_column}] and length $columns[$options{gradebook_column}])?
                             $columns[$options{gradebook_column}]*$options{multiplier}+$options{add}:$columns[$options{gradebook_column}],
			 };
}

my $il = new IO::File $options{ilearn_gradebook},'r' or
     die "Unable to open $options{ilearn_gradebook} for reading: $!";

while (<$il>) {
     chomp;
     my ($name,@columns) = split /\t/;
     my ($id) = $name =~ /\|\s+(\d+)/;
     print $_,qq(\n) and next if not defined $id;
     next if not exists $gb_students{$id};
     $columns[$options{ilearn_column}] = $gb_students{$id}{score} if
	defined $gb_students{$id}{score} and length $gb_students{$id}{score};
     print join("\t",$name,@columns),qq(\n);
}




__END__
